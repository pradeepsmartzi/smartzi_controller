import 'dart:async';
import 'package:dio/dio.dart';
import 'dart:convert';
import 'package:smartzi_controller/model/POJO.dart';

class ConnectApi {
  var dio = Dio();
  String url = "https://test3.smartzi.com/leap-communication/";

  ConnectApi() {
    dio.options.headers["Content-Type"] = "application/json";
  }

  getBookingList(tokenType, accessToken, formData) async {
    dio.options.headers["authorization"] = tokenType + " " + accessToken;
    try {
      Response response = await dio
          .post(url + "admin/tripdetail/cotroller/bookinglist/", data: {
        "page": "1",
        "searchBy": "",
        "status": "",
        "fromDate": null,
        "toDate": null,
        "agentName": "",
        "bookingSource": "",
        "bookingType": "",
        "bookingMode": "",
        "offset": 0,
        "limit": 20
      });
      print(response);
      Map details = jsonDecode(response.toString());
      return TripDetails.fromJson(details);
    } catch (e) {
      print(e);
    }
  }

  getAgent(accessToken, tokenType, userid) async {
    List resBody = List();
    dio.options.headers["authorization"] = accessToken + " " + tokenType;
    try {
      Response response = await dio
          .post("admin/controller/agent/list/", data: {"userId": userid});
      print(response);
      for (int i = 0; i < response.data['data'].length; i++) {
        resBody.add(response.data['data'][i]);
      }
    } catch (e) {
      print(e);
    }
    return resBody;
  }

  getNames() async {
    final response = await dio.get('https://swapi.co/api/people');
    List tempList = new List();
    for (int i = 0; i < response.data['results'].length; i++) {
      tempList.add(response.data['results'][i]);
    }
    return tempList;
  }

  getLoginDetails(username, password) async {
    dio.options.headers["cache-control"] = "no-cache";
    dio.options.headers["postman-token"] = "9efb2bfd-feb5-e635-489c-2d8daf36e290";
    try {
      Response response = await dio.post(url + "public/login/admin/",
          data: {"emailId": username, "password": password});
      print(response);
      Map loginDetails = jsonDecode(response.toString());
      return LoginDetails.fromJson(loginDetails);
    } catch (e) {
      return LoginDetails(null,e.toString(),"401").toJson();
    }
  }

  buildBookingListFormData() {
    FormData formData = new FormData.from({
      "page": "1",
      "searchBy": "",
      "status": "",
      "fromDate": null,
      "toDate": null,
      "agentName": "",
      "bookingSource": "",
      "bookingType": "",
      "bookingMode": "",
      "offset": 0,
      "limit": 20
    });
    return formData;
  }
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'POJO.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User(json['name'] as String, json['email'] as String);
}

Map<String, dynamic> _$UserToJson(User instance) =>
    <String, dynamic>{'name': instance.name, 'email': instance.email};

LoginDetails _$LoginDetailsFromJson(Map<String, dynamic> json) {
  return LoginDetails(
      json['data'] == null
          ? null
          : LoginData.fromJson(json['data'] as Map<String, dynamic>),
      json['message'] as String,
      json['status'] as String);
}

Map<String, dynamic> _$LoginDetailsToJson(LoginDetails instance) =>
    <String, dynamic>{
      'data': instance.data,
      'message': instance.message,
      'status': instance.status
    };

LoginData _$LoginDataFromJson(Map<String, dynamic> json) {
  return LoginData(
      json['oAuthResponse'] == null
          ? null
          : LoginAuth.fromJson(json['oAuthResponse'] as Map<String, dynamic>),
      json['userGUID'] as String,
      json['userId'] as int,
      json['userName'] as String,
      json['emailId'] as String,
      json['referralCode'] as String,
      json['userType'] as int);
}

Map<String, dynamic> _$LoginDataToJson(LoginData instance) => <String, dynamic>{
      'oAuthResponse': instance.oAuthResponse,
      'userGUID': instance.userGUID,
      'userId': instance.userId,
      'userName': instance.userName,
      'emailId': instance.emailId,
      'referralCode': instance.referralCode,
      'userType': instance.userType
    };

LoginAuth _$LoginAuthFromJson(Map<String, dynamic> json) {
  return LoginAuth(
      json['access_token'] as String,
      json['token_type'] as String,
      json['refresh_token'] as String,
      json['expires_in'] as int,
      json['scope'] as String);
}

Map<String, dynamic> _$LoginAuthToJson(LoginAuth instance) => <String, dynamic>{
      'access_token': instance.access_token,
      'token_type': instance.token_type,
      'refresh_token': instance.refresh_token,
      'expires_in': instance.expires_in,
      'scope': instance.scope
    };

TripDetails _$TripDetailsFromJson(Map<String, dynamic> json) {
  return TripDetails(json['data'] as List, json['message'] as String,
      json['status'] as String);
}

Map<String, dynamic> _$TripDetailsToJson(TripDetails instance) =>
    <String, dynamic>{
      'data': instance.data,
      'message': instance.message,
      'status': instance.status
    };

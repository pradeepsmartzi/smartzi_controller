
class BookingListModel {
  final String BookingID;
  final String Date;
  final String Customer;
  final String Driver;
  final String Booking_For;
  final String Booking_Source;
  final int Status;

  const BookingListModel(this.BookingID, this.Date, this.Customer,this.Driver, this.Booking_For, this.Booking_Source,this.Status);
}

import 'package:json_annotation/json_annotation.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'POJO.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class User {
  User(this.name, this.email);

  String name;
  String email;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$UserToJson(this);
}


@JsonSerializable()
class LoginDetails {
  LoginDetails(this.data, this.message, this.status);

  LoginData data;
  String message;
  String status;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory LoginDetails.fromJson(Map<String, dynamic> json) => _$LoginDetailsFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$LoginDetailsToJson(this);
}

@JsonSerializable()
class LoginData {
  LoginData(this.oAuthResponse, this.userGUID, this.userId,this.userName, this.emailId, this.referralCode, this.userType);

  LoginAuth oAuthResponse;
  String userGUID;
  int userId;
  String userName;
  String emailId;
  String referralCode;
  int userType;

  factory LoginData.fromJson(Map<String, dynamic> json) => _$LoginDataFromJson(json);

  Map<String, dynamic> toJson() => _$LoginDataToJson(this);
}

@JsonSerializable()
class LoginAuth {
  LoginAuth(this.access_token, this.token_type, this.refresh_token,this.expires_in, this.scope);

  String access_token;
  String token_type;
  String refresh_token;
  int expires_in;
  String scope;

  factory LoginAuth.fromJson(Map<String, dynamic> json) => _$LoginAuthFromJson(json);

  Map<String, dynamic> toJson() => _$LoginAuthToJson(this);
}

@JsonSerializable()
class TripDetails {
  TripDetails(this.data, this.message, this.status);

  List data;
  String message;
  String status;

  factory TripDetails.fromJson(Map<String, dynamic> json) => _$TripDetailsFromJson(json);

  Map<String, dynamic> toJson() => _$TripDetailsToJson(this);
}

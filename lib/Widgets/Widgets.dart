import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class GetWidget {
  Widget getDropdownWidget(context,data,title,selectedDrpValue,num){
    return new DropdownButton<String>(
      isExpanded: true,
      hint: new Text(title),
      value: selectedDrpValue,
      onChanged: (String newValue) {
        context.setDrp(newValue,num);
      },
      items: data.map<DropdownMenuItem<String>>((user) {
        return new DropdownMenuItem<String>(
          value: user,
          child: new Text(
            user,
            style: new TextStyle(color: Colors.black),
          ),
        );
      }).toList(),
    );
  }

  Widget getTextWidget(title){
    return new Text(title);
  }
  Widget getBoldTextWidget(title){
    return new Text(title,style: TextStyle(fontWeight: FontWeight.bold));
  }
  Widget getTextFormWidget(title,textController){
    return new TextFormField(
      controller: textController,
      decoration: InputDecoration(
          labelText: title
      ),
    );
  }
  var formatter = new DateFormat('yyyy-MM-dd');

  Widget getDatePickerWidget(buildcontext,context,selectedDate,num){
    return new FlatButton(
        shape: RoundedRectangleBorder(
          side: BorderSide(width: 1.0, style: BorderStyle.solid),
          borderRadius: BorderRadius.all(Radius.circular(5.0)),
        ),
        child: Text(selectedDate),
        onPressed: () {
          _showPicker(buildcontext,context,selectedDate,num);
        });
  }

  Future _showPicker(buildcontext,context,selectedDate,num) async {
    DateTime dt;
    try{
      dt = DateTime.parse(selectedDate);
    }catch(e){
      dt = DateTime.now();
    }
    DateTime picked = await showDatePicker(
        context: buildcontext,
        initialDate: dt,
        firstDate: new DateTime(2000),
        lastDate: new DateTime(2050)
    );
    if(picked != null) context.setDate(formatter.format(picked),num);
  }

  Widget getRaisedButtonWidget(context,title){
    return new RaisedButton(
      onPressed: context.btnClick,
      textColor: Colors.white,
      color: Colors.blue,
      padding: const EdgeInsets.all(8.0),
      child: new Text(
        title,
      ),
    );
  }
}

//Dismissable slide menu

class SlideMenu extends StatefulWidget {
  final Widget child;
  final List<Widget> menuItems;

  SlideMenu({this.child, this.menuItems});

  @override
  _SlideMenuState createState() => new _SlideMenuState();
}

class _SlideMenuState extends State<SlideMenu> with SingleTickerProviderStateMixin {
  AnimationController _controller;

  @override
  initState() {
    super.initState();
    _controller = new AnimationController(vsync: this, duration: const Duration(milliseconds: 200));
  }

  @override
  dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final animation = new Tween(
        begin: const Offset(0.0, 0.0),
        end: const Offset(-0.2, 0.0)
    ).animate(new CurveTween(curve: Curves.decelerate).animate(_controller));

    return new GestureDetector(
      onHorizontalDragUpdate: (data) {
        // we can access context.size here
        setState(() {
          _controller.value -= data.primaryDelta / context.size.width;
        });
      },
      onHorizontalDragEnd: (data) {
        if (data.primaryVelocity > 2500)
          _controller.animateTo(.0); //close menu on fast swipe in the right direction
        else if (_controller.value >= .5 || data.primaryVelocity < -2500) // fully open if dragged a lot to left or on fast swipe to left
          _controller.animateTo(1.0);
        else // close if none of above
          _controller.animateTo(.0);
      },
      child: new Stack(
        children: <Widget>[
          new SlideTransition(position: animation, child: widget.child),
          new Positioned.fill(
            child: new LayoutBuilder(
              builder: (context, constraint) {
                return new AnimatedBuilder(
                  animation: _controller,
                  builder: (context, child) {
                    return new Stack(
                      children: <Widget>[
                        new Positioned(
                          right: .0,
                          top: .0,
                          bottom: .0,
                          width: constraint.maxWidth * animation.value.dx * -1,
                          child: new Container(
                            color: Colors.black26,
                            child: new Row(
                              children: widget.menuItems.map((child) {
                                return new Expanded(
                                  child: child,
                                );
                              }).toList(),
                            ),
                          ),
                        ),
                      ],
                    );
                  },
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
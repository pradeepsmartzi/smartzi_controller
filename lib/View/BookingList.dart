import 'package:flutter/material.dart';
import 'package:smartzi_controller/ViewModel/BookingList.dart';
import 'package:smartzi_controller/model/BookingListModel.dart';
import 'package:smartzi_controller/Util/Constants.dart';
import 'package:smartzi_controller/Widgets/Widgets.dart';

class BookingListView extends BookingListState {
  double rating = 3.5;

  @override
  void initState() {
    super.initState();
    setListner();
    getBookingList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildBar(context),
      body: _buildList(),
      resizeToAvoidBottomPadding: false,
    );
  }

  Widget _buildBar(BuildContext context) {
    return new AppBar(centerTitle: true, title: appBarTitle, actions: <Widget>[
      new IconButton(
        icon: searchIcon,
        onPressed: searchPressed,
      ),
      new IconButton(
        icon: new Icon(Icons.filter),
        onPressed: filterAction,
      ),
    ]);
  }

  Widget _buildList() {
    filterItem();
    return _showList();
  }

  listitem(i) {
    if (i == 0) {
      return <Widget>[
        new Expanded(child: GetWidget().getBoldTextWidget(Constants.BookingID)),
        new Expanded(child: GetWidget().getBoldTextWidget(Constants.Date)),
        new Expanded(child: GetWidget().getBoldTextWidget(Constants.Customer)),
        new Expanded(child: GetWidget().getBoldTextWidget(Constants.Driver)),
      ];
    }
    print(filteredBookingList[i - 1]["bookingId"]);
    return <Widget>[
      new Expanded(child: GetWidget().getTextWidget(filteredBookingList[i - 1]["bookingId"])),
      new Expanded(child: GetWidget().getTextWidget(filteredBookingList[i - 1]["travelDateTime"])),
      new Expanded(child: GetWidget().getTextWidget(filteredBookingList[i - 1]["customerName"])),
      new Expanded(child: GetWidget().getTextWidget(filteredBookingList[i - 1]["driverName"])),
    ];
  }

  menuItem() {
    return <Widget>[
      new PopupMenuButton<String>(
        onSelected: listMenuAction,
        tooltip: Constants.Menu,
        itemBuilder: (BuildContext context) {
          return Constants.booking_Listtile_menu.map((String choice) {
            return PopupMenuItem<String>(
              value: choice,
              child: Text(choice),
            );
          }).toList();
        },
      )
    ];
  }

  listRow(i) {
    if (i == 0) {
      return ListTile(
        title: Row(children: listitem(i)),
      );
    } else {
      return new SlideMenu(
          child: new ListTile(
            title: Row(children: listitem(i)),
          ),
          menuItems: menuItem());
    }
  }

  _showList() {
    int listSize = filteredBookingList.length;
    return new ListView(
      children: ListTile.divideTiles(
        context: context,
        tiles: new List.generate(listSize + 1, (index) {
          return listRow(index);
        }),
      ).toList(),
    );
  }
}

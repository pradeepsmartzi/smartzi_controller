import 'package:flutter/material.dart';
import 'package:smartzi_controller/ViewModel/BookingListFilter.dart';
import 'package:smartzi_controller/Widgets/Widgets.dart';
import 'package:smartzi_controller/Util/Constants.dart';

class BookingListView extends BookingListFilterState {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
          backgroundColor: Colors.white.withOpacity(0.85),
          body: new Center(
        child: Padding(
          padding: const EdgeInsets.all(18.0),
          child: new Card(
            elevation: 1.0,
            margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  GetWidget().getBoldTextWidget(Constants.BookingAgent),
                  GetWidget().getDropdownWidget(
                      this, agents, Constants.SelectAgent, selectedDrpValue, 0),
                  GetWidget().getBoldTextWidget(Constants.Status),
                  GetWidget().getDropdownWidget(this, agents,
                      Constants.SelectStatus, selectedDrpValue1, 1),
                  GetWidget().getBoldTextWidget(Constants.BookingSource),
                  GetWidget().getDropdownWidget(this, agents,
                      Constants.SelectSource, selectedDrpValue2, 2),
                  GetWidget().getBoldTextWidget(Constants.FromDate),
                  GetWidget()
                      .getDatePickerWidget(context, this, selectedDate, 0),
                  GetWidget().getBoldTextWidget(Constants.ToDate),
                  GetWidget()
                      .getDatePickerWidget(context, this, selectedDate1, 1),
                  GetWidget().getRaisedButtonWidget(this, "Apply Filter"),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    addAgentData();
  }
}

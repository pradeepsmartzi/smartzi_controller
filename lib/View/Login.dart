import 'package:flutter/material.dart';
import 'package:smartzi_controller/ViewModel/Login.dart';
import 'package:smartzi_controller/Widgets/Widgets.dart';
import 'package:smartzi_controller/Util/Constants.dart';

class LoginView extends LoginState {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
        home: new Scaffold(
          backgroundColor: Colors.deepOrangeAccent,
          body: new Center(
            child: Padding(
              padding: const EdgeInsets.all(18.0),
              child: new Card(
                elevation: 1.0,
                margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      GetWidget().getBoldTextWidget(Constants.Login),
                      GetWidget().getTextFormWidget(Constants.Username,emailController),
                      GetWidget().getTextFormWidget(Constants.Password,pwdController),
                      Padding(
                        padding: const EdgeInsets.only(top: 12.0),
                        child: GetWidget().getRaisedButtonWidget(this, Constants.SignIn),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ));
  }
}

import 'package:flutter/material.dart';
import 'package:smartzi_controller/View/BookingList.dart';
import 'package:smartzi_controller/model/BookingListModel.dart';
import 'package:smartzi_controller/Api/ConnectApi.dart';
import 'package:smartzi_controller/Util/Constants.dart';
import 'package:smartzi_controller/ViewModel/BookingListFilter.dart';
import 'package:smartzi_controller/Widgets/Widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BookingList extends StatefulWidget {
  @override
  BookingListView createState() => new BookingListView();
}

abstract class BookingListState extends State<BookingList> {
  @protected
  List bookingListItem = new List<Map>();

  @protected
  getBookingList() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    var value = await ConnectApi().getBookingList(prefs.getString(Constants.tokenType),prefs.getString(Constants.accessToken),ConnectApi().buildBookingListFormData());
    setState(() {
      print(filteredBookingList.length );
      bookingListItem = value.data;
      filteredBookingList = bookingListItem;
      print(bookingListItem[0]["customerName"]);
      print(filteredBookingList.length );
    });
  }

  void listMenuAction(String choice) {
//    if (choice == Constants.Search) {
////      _searchPressed;
//    } else if (choice == Constants.Filter) {
////      Navigator.of(context).push(PageRouteBuilder(
////        opaque: false,
////        pageBuilder: (context,_,__) => BookingListFilter(),
////      ));
//      Navigator.of(context).push(new MaterialPageRoute<Null>(
//          builder: (BuildContext context) {
//            return new BookingListFilter();
//          },
//          fullscreenDialog: true));
////      showDialog(
////          context: context,
////          builder: (_) => Material(
////            type: MaterialType.transparency,
////
////            child: Center( // Aligns the container to center
////                child: Container( // A simplified version of dialog.
////                  width: 100.0,
////                  height: 56.0,
////                  color: Colors.green,
////                  child: Text('jojo'),
////                )
////            ),
////          )
////      );
//    }
  }

  @protected
  List names = new List();
  @protected
  List filteredNames = new List();
  @protected
  List filteredBookingList  = new List<Map>();
  @protected
  final TextEditingController filter = new TextEditingController();
  @protected
  String searchText = "";
  @protected
  Icon searchIcon = new Icon(Icons.search);
  @protected
  Widget appBarTitle = GetWidget().getTextWidget(Constants.BookingList);

  @protected
  void filterItem() {
    if (!(searchText.isEmpty)) {
      var tempList;
      for (int i = 0; i < bookingListItem.length; i++) {
        if (bookingListItem[i]["bookingId"]
                .toLowerCase()
                .contains(searchText.toLowerCase()) ||
            bookingListItem[i]["travelDateTime"]
                .toLowerCase()
                .contains(searchText.toLowerCase()) ||
            bookingListItem[i]["customerName"]
                .toLowerCase()
                .contains(searchText.toLowerCase()) ||
            bookingListItem[i]["driverName"]
                .toLowerCase()
                .contains(searchText.toLowerCase())) {
          tempList.add(bookingListItem[i]);
        }
      }
      setState(() {
        filteredBookingList = tempList;
      });
    }
  }

  void searchPressed() {
    setState(() {
      if (this.searchIcon.icon == Icons.search) {
        this.searchIcon = new Icon(Icons.close);
        this.appBarTitle = new TextField(
          controller: filter,
          decoration: new InputDecoration(
              prefixIcon: new Icon(Icons.search), hintText: 'Search...'),
        );
      } else {
        this.searchIcon = new Icon(Icons.search);
        this.appBarTitle = GetWidget().getTextWidget(Constants.BookingList);
        filteredBookingList = bookingListItem;
        filter.clear();
      }
    });
  }

  void filterAction() {
//    Navigator.of(context).push(MaterialPageRoute(
//      builder: (context) => BookingListFilter(),
//    ));
    Navigator.of(context).push(new MaterialPageRoute<Null>(
        builder: (BuildContext context) {
          return new BookingListFilter();
        },
        fullscreenDialog: true));
  }

  setListner() {
    filter.addListener(() {
      if (filter.text.isEmpty) {
        setState(() {
          searchText = "";
          filteredBookingList = bookingListItem;
        });
      } else {
        setState(() {
          searchText = filter.text;
        });
      }
    });
  }
}

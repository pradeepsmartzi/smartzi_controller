import 'package:flutter/material.dart';
import 'package:smartzi_controller/View/Login.dart';
import 'package:smartzi_controller/ViewModel/BookingList.dart';
import 'package:smartzi_controller/Api/ConnectApi.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smartzi_controller/Util/Constants.dart';
import 'package:crypto/crypto.dart';
import 'dart:convert';

class Login extends StatefulWidget {
  @override
  LoginView createState() => new LoginView();
}

abstract class LoginState extends State<Login> {

  @protected
  var agents = List();
  TextEditingController emailController = new TextEditingController();
  TextEditingController pwdController = new TextEditingController();

  @protected
  void btnClick() async {
    var bytes = utf8.encode(pwdController.text);
    var digest = sha256.convert(bytes);
    var data = await ConnectApi().getLoginDetails(emailController.text,"$digest");
    if(data.status == "401"){
      Scaffold.of(context).showSnackBar(new SnackBar(
        content: new Text(Constants.Error),
      ));
    }else if(data.status == "1000"){
      SharedPreferences prefs = await SharedPreferences.getInstance();
      print(data.data.userId);
      prefs.setInt(Constants.Userid, data.data.userId);
      prefs.setString(
          Constants.accessToken, data.data.oAuthResponse.access_token);
      prefs.setString(Constants.tokenType, data.data.oAuthResponse.token_type);
      Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => BookingList(),
      ));
    }else{
      Scaffold.of(context).showSnackBar(new SnackBar(
        content: new Text(data.message),
      ));
    }
  }

}

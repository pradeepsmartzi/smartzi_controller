import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:smartzi_controller/View/BookingListFilter.dart';
import 'package:smartzi_controller/Api/ConnectApi.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smartzi_controller/Util/Constants.dart';

class BookingListFilter extends StatefulWidget {
  @override
  BookingListView createState() => new BookingListView();
}

abstract class BookingListFilterState extends State<BookingListFilter> {

  @protected
  String selectedDrpValue,selectedDrpValue1,selectedDrpValue2;

  @protected
  String selectedDate = "From Date",selectedDate1 = "To Date";

  @protected
  var agents = List();

  @protected
  void setDrp(text,num) {
    setState((){
      switch(num){
        case 0:
          selectedDrpValue = text;
          break;
        case 1:
          selectedDrpValue1 = text;
          break;
        case 2:
          selectedDrpValue2 = text;
          break;
      }
    });
  }

  @protected
  void setDate(text,num) {
    setState((){
      switch(num){
        case 0:
          selectedDate = text;
          break;
        case 1:
          selectedDate1 = text;
          break;
      }
    });
  }
  @protected
  void btnClick() async {
//    if (Navigator.canPop(context)) {
      Navigator.pop(context);
//    } else {
//      SystemNavigator.pop();
//    }
  }

  @protected
  Future addAgentData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var data = await ConnectApi().getAgent(prefs.getInt(Constants.Userid),prefs.getString(Constants.accessToken),prefs.getString(Constants.tokenType));
    setState(() =>  agents = data);
  }

}

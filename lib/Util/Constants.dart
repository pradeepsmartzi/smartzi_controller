class Constants{

  static const String AppName = 'Controller';
  static const String BookingList = 'BookingList';
  static const String BookingID = 'BookingID';
  static const String Date = 'Date';
  static const String Customer = 'Customer';
  static const String Driver = 'Driver';
  static const String BookingAgent = "Booking Agent";
  static const String Status = "Status";
  static const String BookingSource = "Booking Source";
  static const String FromDate = "From Date";
  static const String ToDate = "To Date";
  static const String SelectAgent = "Select Agent";
  static const String SelectStatus = "Select Status";
  static const String SelectSource = "Select Source";
  static const String Login = 'Login';
  static const String Username = 'Username';
  static const String Password = 'Password';
  static const String SignIn = 'SIGN IN';
  static const String Userid = 'userId';
  static const String accessToken = 'accessToken';
  static const String tokenType = 'tokenType';
  static const String Error = 'Error occured.Try Again.';
  static const String Failed = 'Failed';

  static const String View = 'View';
  static const String Edit = 'Edit';
  static const String Reassign = 'Reassign';
  static const String EndTrip = 'EndTrip';
  static const String Cancel = 'Cancel';

  static const List<String> booking_Listtile_menu = <String>[
    View,
    Edit,
    Reassign,
    EndTrip,
    Cancel
  ];

  static const String Menu = 'Menu';
  static const String Search = 'Search';
  static const String Filter = 'Filter';

  static const List<String> booking_List_menu = <String>[
    Search,
    Filter
  ];

  static const String Subscribe = 'Subscribe';
  static const String Settings = 'Settings';
  static const String SignOut = 'Sign out';

  static const List<String> choices = <String>[
    Subscribe,
    Settings,
    SignOut
  ];
}